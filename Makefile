MAKEFLAGS += --silent
.DEFAULT_GOAL := run

s3_bucket    := bedrock-tfstate
dynamodb     := bedrock-tfstate
oidc_role    := bedrock-terraform-oidc
assume_role  := bedrock-tf-state
deploy_role  := bedrock-terraform
oidc_idp     := gitlab.com
account_id   :=

RED    := \033[0;31m
BLUE   := \033[0;34m
GREEN  := \033[0;32m
YELLOW := \033[0;33m
NC     := \033[0m

TF_DIR = tf

.PHONY: run
run: parse-args check-account-id output-params import

.PHONY: parse-args
parse-args:
	$(eval ARGS := $(filter-out $@,$(MAKECMDGOALS)))
	$(foreach arg,$(ARGS),$(call set-param,$(arg)))

define set-param
	$(eval arg := $(1))
	$(if $(filter -arg1%,$(arg)),$(eval s3_bucket := $(patsubst -arg1%,%,$(arg))))
	$(if $(filter -arg2%,$(arg)),$(eval dynamodb := $(patsubst -arg2%,%,$(arg))))
	$(if $(filter -arg3%,$(arg)),$(eval oidc_role := $(patsubst -arg3%,%,$(arg))))
	$(if $(filter -arg4%,$(arg)),$(eval assume_role := $(patsubst -arg4%,%,$(arg))))
	$(if $(filter -arg5%,$(arg)),$(eval deploy_role := $(patsubst -arg4%,%,$(arg))))
	$(if $(filter -arg6%,$(arg)),$(eval account_id := $(patsubst -arg4%,%,$(arg))))
endef

.PHONY: check-account-id
check-account-id:
	$(call check-parameter,account_id)

define check-parameter
	echo "Checking parameter: $(1)"
	@if [ -z "$(strip $($(1)))" ]; then \
		echo "$(RED)$(1) parameter is not specified. Please provide the $(1) parameter.$(NC)"; \
		echo "Usage: make account_id=<account_id>"                                                              ; \
		echo "Additional Arguments:"                                                                            ; \
		echo "  s3_bucket=<s3_bucket>      : Specify Terraform State S3 Bucket (default: uniqueprefix-tfstate)" ; \
		echo "  dynamodb=<dynamodb>        : Specify Terraform State DynamoDB (default: bedrock-tfstate)"    ; \
		echo "  oidc_role=<oidc_role>      : Specify AWS Entry Role (default: bedrock-terraform-oidc)"          ; \
		echo "  assume_role=<assume_role>  : Specify AWS State Role (default: bedrock-tf-state)"                ; \
		echo "  deploy_role=<deploy_role>  : Sepcify AWS Deployment Role (default: bedrock-terraform)"          ; \
		echo "  account_id=<account_id>    : Specify AWS Management Account ID (required, must be 12 digits)"   ; \
		exit 1; \
	fi
	@if ! echo "$($(1))" | grep -Eq '^[0-9]{12}$$'; then \
		echo "$(RED)$(1) parameter should have exactly 12 digits.$(NC)"; \
		echo "Usage: make account_id=<account_id>"                                                              ; \
		echo "Additional Arguments:"                                                                            ; \
		echo "  s3_bucket=<s3_bucket>      : Specify Terraform State S3 Bucket (default: uniqueprefix-tfstate)" ; \
		echo "  dynamodb=<dynamodb>        : Specify Terraform State DynamoDB (default: bedrock-tfstate)"    ; \
		echo "  oidc_role=<oidc_role>      : Specify AWS Entry Role (default: bedrock-terraform-oidc)"          ; \
		echo "  assume_role=<assume_role>  : Specify AWS State Role (default: bedrock-tf-state)"                ; \
		echo "  deploy_role=<deploy_role>  : Sepcify AWS Deployment Role (default: bedrock-terraform)"          ; \
		echo "  account_id=<account_id>    : Specify AWS Management Account ID (required, must be 12 digits)"   ; \
		exit 1; \
	fi
endef

.PHONY: output-params
output-params:
	echo "AWS Management Account ID (account_id)   : ${BLUE}$(account_id)${NC}"
	echo
	echo "Terraform State S3 Bucket (s3_bucket)    : ${GREEN}$(s3_bucket)${NC}"
	echo "Terraform State DynamoDB  (dynamodb)     : ${YELLOW}$(dynamodb)${NC}"
	echo "AWS OIDC Entry Role       (oidc_role)    : ${BLUE}$(oidc_role)${NC}"
	echo "AWS State Access Role     (assume_role)  : ${BLUE}$(assume_role)${NC}"
	echo "AWS Deployment Role       (deploy_role)  : ${BLUE}$(deploy_role)${NC}"
	echo
	echo

%:
	@:


.PHONY: init
init:
	@if [ ! -d "$(TF_DIR)/.terraform" ]; then \
		echo "$(YELLOW)=============================${NC}";\
		echo "$(YELLOW)Initalising Bootstrap Project${NC}";\
		echo "$(YELLOW)=============================${NC}";\
		terraform -chdir=$(TF_DIR) init;\
	fi

.PHONY: import
import: init
	echo "$(YELLOW)=============================${NC}"
	echo "$(YELLOW)Importing Bootstrap Resources${NC}"
	echo "$(YELLOW)=============================${NC}"
	echo

	echo "$(BLUE)Checking DynamoDB Locking Table Resource${NC}"
	cd $(TF_DIR) && \
		terraform state show aws_dynamodb_table.terraform                               > /dev/null 2>&1 || terraform import aws_dynamodb_table.terraform ${dynamodb}

	echo "$(BLUE)Checking S3 Bucket and Configuration Resources${NC}"
	cd $(TF_DIR) && \
		terraform state show aws_s3_bucket.tfstate                                      > /dev/null 2>&1 || terraform import aws_s3_bucket.tfstate                                      ${s3_bucket}; \
		terraform state show aws_s3_bucket_acl.tfstate                                  > /dev/null 2>&1 || terraform import aws_s3_bucket_acl.tfstate                                  ${s3_bucket}; \
		terraform state show aws_s3_bucket_lifecycle_configuration.tfstate              > /dev/null 2>&1 || terraform import aws_s3_bucket_lifecycle_configuration.tfstate              ${s3_bucket}; \
		terraform state show aws_s3_bucket_policy.tfstate                               > /dev/null 2>&1 || terraform import aws_s3_bucket_policy.tfstate                               ${s3_bucket}; \
		terraform state show aws_s3_bucket_public_access_block.tfstate                  > /dev/null 2>&1 || terraform import aws_s3_bucket_public_access_block.tfstate                  ${s3_bucket}; \
		terraform state show aws_s3_bucket_server_side_encryption_configuration.tfstate > /dev/null 2>&1 || terraform import aws_s3_bucket_server_side_encryption_configuration.tfstate ${s3_bucket}; \
		terraform state show aws_s3_bucket_versioning.tfstate                           > /dev/null 2>&1 || terraform import aws_s3_bucket_versioning.tfstate                           ${s3_bucket}; 

	echo "$(BLUE)Checking OIDC IAM Role Resources${NC}"
	cd $(TF_DIR) && \
		terraform state show aws_iam_role.foundation_oidc                               > /dev/null 2>&1 || terraform import aws_iam_role.foundation_oidc                               ${oidc_role}; \
		terraform state show 'aws_iam_openid_connect_provider.gitlab[0]'                > /dev/null 2>&1 || terraform import 'aws_iam_openid_connect_provider.gitlab[0]' arn:aws:iam::${account_id}:oidc-provider/${oidc_idp}; \
		terraform state show aws_iam_role.tf_state                                      > /dev/null 2>&1 || terraform import aws_iam_role.tf_state                                      ${assume_role}; \
		terraform state show aws_iam_role.bedrock                                       > /dev/null 2>&1 || terraform import aws_iam_role.bedrock                                       ${deploy_role}

	echo "$(YELLOW)==========================${NC}"
	echo "$(YELLOW)Completed Bootstrap Import${NC}"
	echo "$(YELLOW)==========================${NC}"

.PHONY: verify-bootstrap
verify-bootstrap: init import
	cd $(TF_DIR) && \
		terraform refresh && terraform plan

.PHONY: apply-bootstrap
apply-bootstrap: verify-bootstrap
	cd $(TF_DIR) && \
		terraform apply -auto-approve

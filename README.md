# bedrock-api-accelerator

## Prerequisites

- [Terraform](https://www.terraform.io/downloads.html) installed locally.

## Usage
### Bootstrap Instructions
awsume / set AWS_PROFILE to your temp IAM admin/SSO user in the managment account
set the variables for your CI/CD providers. 

To use this project, follow the steps below:

1. Clone the repository.

2. Set the required parameters by passing them as arguments to the Makefile.

    ```
    make account_id=<account_id> \
      s3_bucket=<s3_bucket>      \ 
      dynamodb=<dynamodb>        \
      oidc_role=<oidc_role>      \
      assume_role=<assume_role>  \
      deploy_role=<deploy_role>
    ```

    Additional Arguments:

    - `account_id`: Specify the AWS Management Account ID (*required*, must be 12 digits).
    - `s3_bucket`: Specify the Terraform State S3 Bucket (default: uniqueprefix-tfstate).
    - `dynamodb`: Specify the Terraform State DynamoDB (default: bedrock-tfstate).
    - `oidc_role`: Specify the AWS Entry Role (default: bedrock-terraform-oidc).
    - `assume_role`: Specify the AWS State Role (default: bedrock-tf-state).
    - `deploy_role`: Specify the AWS Deployment Role (default: bedrock-terraform).

3. Run the bootstrap process.

    ```
    make account_id=086836442336 apply-bootstrap
    ```

## Available Targets

- `run`: Parses arguments, checks the account ID, and runs the import.
- `verify-bootstrap`: Verifies the imported bootstrap resources.
- `apply-bootstrap`: Applies the bootstrap changes.

## Configuration

The following configuration parameters can be set as arguments when running the Makefile:

- `account_id`: The AWS Management Account ID.
- `s3_bucket`: The Terraform State S3 Bucket (default: uniqueprefix-tfstate).
- `dynamodb`: The Terraform State DynamoDB (default: bedrock-tfstate).
- `oidc_role`: The AWS Entry Role (default: bedrock-terraform-oidc).
- `assume_role`: The AWS State Role (default: bedrock-tf-state).
- `deploy_role`: The AWS Deployment Role (default: bedrock-terraform).

## Color Codes

The Makefile output uses color codes for better readability

Note: The color codes might not be visible in all terminals or text editors.

module "modules_all_global" {
  source        = "../global/foundation_baseline"
  alias_name    = lower(local.workspace["tags"]["environment"])
  unique_prefix = local.workspace["unique_prefix"]
}

variable "state" {
  type        = map(string)
  description = "State Object location of the Org remote state"
}

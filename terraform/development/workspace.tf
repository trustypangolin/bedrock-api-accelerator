# Default Variables for all workspaces
locals {
  default = {
    # Remote State Variable Lookups for Default Workspace
    base_region   = data.terraform_remote_state.org.outputs.base_region
    base_role     = data.terraform_remote_state.org.outputs.base_role
    unique_prefix = data.terraform_remote_state.org.outputs.unique_prefix

    # Account ID Lookups for All accounts - Note, there must be permissions to actually access the role, this isn't given by default
    base_account = data.terraform_remote_state.org.outputs.acc[data.terraform_remote_state.org.outputs.acc_map["Development"]]

    # Local Customisations for this Project
    tags = {
      Service     = "Bedrock",
      Version     = "3.0.0"
      Environment = "Development" # user lower(tags["Environment"]) for resource naming
    }
  }
}

locals {
  # A workaround based on https://github.com/hashicorp/terraform/issues/15966
  workspaces = merge(
    local.default,
    local.bedrock_demo, # Bedrock Demo env file
  )

  workspace = terraform.workspace == "default" ? local.default : merge(local.default, local.workspaces[terraform.workspace])
}

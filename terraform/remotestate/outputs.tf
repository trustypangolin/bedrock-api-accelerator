# This is cutdown version of the Bedrock Landing Zone Outputs
# Generally you would use that instead of this project, but not everyone has a Bedrock Landing Zone

# This is how we get the account numbers and account names to all the other folders, 
# we export these, and pull it from the org state file directly, eg providers
output "acc" {
  value = {
    for k in data.aws_organizations_organization.org.accounts[*] :
    k.name => k.id
  }
}

# Typically, AWS accounts don't always have nice consistant names, we map the real name to the functional lookup name.
output "acc_map" {
  value = local.workspace["acc_map"]
}

# Base Region for AWS
output "base_region" {
  value = local.workspace["base_region"]
}

# Role name used to assume child accounts wth Admin Privs
output "base_role" {
  value = local.workspace["base_role"]
}

# Customer Prefix
output "unique_prefix" {
  value = local.workspace["unique_prefix"]
}

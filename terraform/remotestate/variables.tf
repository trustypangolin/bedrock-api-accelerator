# Mandatory, you must set this somewhere
variable "unique_prefix" {
  type        = string
  description = "prefix used to be part of resource name"
}

variable "acc_map" {
  description = "Account Name Mappings where the Account Name differs from the recommended names"
  type        = map(string)
  default = {
    Management  = "Management"
    Development = "Development"
    Production  = "Production"
  }
}

# Org Variables
variable "base_role" {
  type        = string
  description = "Sub Account Role to create/assume."
  default     = "OrganizationAccountAccessRole" # Non ControlTower default
}

variable "base_region" {
  type        = string
  description = "AWS region to operate in. Defaults to ap-southeast-2 (Sydney)."
  default     = "ap-southeast-2"
}
